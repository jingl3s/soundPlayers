#-*-coding:utf8;-*-
# qpy:3
'''
@author: zorbac
'''

# license
#
# Producer 2017 zorbac at free.fr
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).

try:
    from sounds.configuration.configuration_sounds import ConfigurationSounds
    from sequences.sequences_gen import obtenir_list_seq

except Exception as e:
    print(e)


class LoaderInterraction(object):
    '''
    '''

    def __init__(self, load_seq=False):
        print('----------------')
        self._elements = list()
        self._next_index = 0
        self._config = ConfigurationSounds()
        self._config.load_configs()
        self._update_elements()
        if load_seq:
            directory = self._config.get_configs_path()
            self._list_seq = obtenir_list_seq(directory, interact=self)
        else:
            self._list_seq = None

    @staticmethod
    def get_interraction(load_seq=False):
        return LoaderInterraction(load_seq)

    def get_number_sounds(self):
        return len(self._elements)

    def play_sound(self, index):
        '''
        Si index supérieur au nombre de morceaux disponible, utilisation du modulo pour restreindre la valeur
        :param index:
        '''
        print('play_sound : {}'.format(index))
        # Permet d'éviter une erreur si plus de bouton que disponibles
        if index >= self.get_number_sounds():
            _index = index % self.get_number_sounds()
        else:
            _index = index
        self._next_index = _index
        self._elements[_index].play()
#         Prepare son suivant si disponible
        self._inc_next()
        self._update_id_seq(_index)

    def play_next(self):
        self._elements[self._next_index].play()
        self._update_id_seq(self._next_index)
        self._inc_next()

    def play_next_no_seq(self):
        self._elements[self._next_index].play()
        self._inc_next()

    def _inc_next(self):
        self._next_index += 1
        if self._next_index == len(self._elements):
            self._next_index = 0

    def set_new_config(self, option):
        self._config.set_new_config(option)
        self._update_elements()

    def reload_configs(self):
        self._config.load_configs()
        self._update_elements()

    def get_configs_name(self):
        return self._config.get_configs_name()

    def get_current_config(self):
        return self._config.get_current_config()

    def change_next_config(self):
        new_config = self._config.get_next_config()
        self.set_new_config(new_config)

    def _update_elements(self):
        self._elements = self._config.get_config()

    def _update_id_seq(self, id):
        if self._list_seq is not None:
            for seq in self._list_seq:
                seq.event(id)


if __name__ == '__main__':
    interaction = LoaderInterraction()
    print(interaction.get_number_sounds())
    interaction.play_sound(0)
