#-*-coding:utf8;-*-
# qpy:3
'''
@author: zorbac

'''

# license
#
# Producer 2017 zorbac at free.fr
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).

import os
from sounds.sound_players import sound_player_factory
from common import configuration_loader


class Configuration(object):
    '''
    classdocs
    '''

    def __init__(self, path_json_file):
        '''
        Constructor
        '''
        self._dossier_fichier_config = (os.path.split(path_json_file))[0]
        file = (os.path.split(path_json_file))[1]

        gestion_config = configuration_loader.ConfigurationLoader(
            self._dossier_fichier_config)

        dossier = os.path.realpath(os.path.dirname(__file__))
        gestion_config.set_chemin_configuration_default(dossier)

        gestion_config.set_configuration_file_name(file)
        self._config_json = gestion_config.obtenir_configuration()
        self._elements = list()
        for value in sorted(self._config_json):
            if self._config_json[value]["TYPE"] == sound_player_factory.SoundPlayerTypes.SOUND_FILE:
                contenu = os.path.join(self._dossier_fichier_config,
                                       self._config_json[value]["FICHIER"])
            else:
                contenu = self._config_json[value]["FICHIER"]

            player1 = sound_player_factory.SoundPlayerFactory()\
                .creer_player(self._config_json[value]["TYPE"],
                              contenu)
            self._elements.append(player1)

    def get_config_sound(self):
        return self._elements


if __name__ == '__main__':
    try:
        config = Configuration(
            "/mnt/1T/01_Documents_Secondaires/Programmation/PythonScripts/devSounds/configs/config_nintendo/config_nintendo.json")
        elements = config.get_config_sound()
        print(elements)
    except Exception as e:
        print(e)
