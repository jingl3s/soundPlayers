#-*-coding:utf8;-*-
# qpy:3
'''
@author: zorbac

'''

# license
#
# Producer 2017 zorbac at free.fr
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).

import os
import json
from sounds.configuration.configuration import Configuration


class ConfigurationSounds(object):
    '''
    classdocs
    '''
    DOSSIER_CONFIGS = os.path.join((os.path.split(__file__))[0],
                                   "..", "..",
                                   "configs", "sounds")

    FILE_SAVE = os.path.join(DOSSIER_CONFIGS,
                             'configs.json')
    BASE_NOM_DOSSIER = "config_"

    def __init__(self):
        '''
        Constructor
        '''
        self._configs = dict()
        self._current_config = None

    def get_configs_name(self):
        return self._configs.keys()

    def get_configs_path(self):
        return self.DOSSIER_CONFIGS

    def get_current_config(self):
        return self._current_config

    def get_next_config(self):
        list_config = list(self._configs.keys())
        list_config.sort()
        idx = list_config.index(self._current_config)
        print (idx)
        idx += 1
        if idx >= len(list_config):
            idx = 0
        return list_config[idx]

    def set_new_config(self, config_name):
        if self._current_config != config_name:
            self._save_new_config(config_name)

    def _save_new_config(self, config_name):
        try:
            self._current_config = config_name
            content = dict()
            content['selection'] = self._current_config
            with open(self.FILE_SAVE, 'w') as f:
                json.dump(content, f)
        except Exception as e:
            raise Exception(e)

    def get_config(self):
        return self._configs[self._current_config]

    def load_configs(self):
        list_folders = os.listdir(self.DOSSIER_CONFIGS)
        list_dossier_filtre = [
            dossier
            for dossier in list_folders
            if self.BASE_NOM_DOSSIER in dossier]

        for dossier in list_dossier_filtre:
            if os.path.isdir(os.path.join(self.DOSSIER_CONFIGS, dossier)):
                fichier_config = os.path.join(
                    self.DOSSIER_CONFIGS, dossier, dossier + ".json")
                config = Configuration(fichier_config)
                self._configs[
                    dossier.replace(self.BASE_NOM_DOSSIER, "")] = config.get_config_sound()

        self._load_selected_config()
        if self._current_config is None:
            self._current_config = self._configs.keys()[0]

    def _load_selected_config(self):
        try:
            with open(self.FILE_SAVE, 'r') as f:
                json_config_selected = json.load(f)
                self._current_config = json_config_selected['selection']
        except Exception as e:
            print(e)
            self._current_config = None

#{
#	"selection": "config_nintendo"
#}


if __name__ == '__main__':
    config = ConfigurationSounds()
    config.load_configs()
    print(config.get_config())
