#-*-coding:utf8;-*-
# qpy:3
'''
@author: zorbac

Speak user generated text.
'''

# license
#
# Producer 2017 zorbac at free.fr
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).

import subprocess


class Sound(object):

    def __init__(self, message):
        self._message = message
        self._cmd = ["termux-media-player", "play", self._message]

    def play(self):
        print(" ".join(self._cmd))
        player = subprocess.Popen(
            self._cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output = player.communicate()

    def set_msg(self, message):
        self._message = message
