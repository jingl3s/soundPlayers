#-*-coding:utf8;-*-
# qpy:3
'''
@author: zorbac
'''

# license
#
# Producer 2017 zorbac at free.fr
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).


class AfficheText(object):
    '''
    Affiche du texte dans la console
    '''

    def __init__(self, message):
        self._message = message

    def play(self):
        print(self._message)

    def set_msg(self, message):
        self._message = message


if __name__ == "__main__":
    msg = "coucou"
    print("debut")
    try:
        text = AfficheText(msg)
        text.play()
    except Exception as e:
        print(e)
    finally:
        print("fin")
    exit(0)
