#-*-coding:utf8;-*-
# qpy:3
'''
@author: zorbac

'''

# license
#
# Producer 2017 zorbac at free.fr
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).

from sounds.sound_interraction import LoaderInterraction
from rpi.configuration_rpi import ConfigurationRpi
#from rpi.rpi_boutons import RpiBoutons
from rpi.rpi_boutons2 import RpiBoutons
import os
from sequences.sequences_gen import obtenir_list_seq


def lance():
    # Configuration
    dossier_config = os.path.join(os.path.dirname(__file__),
                                  "..",
                                  "configs",
                                  "rpi")

    path_config = os.path.join(dossier_config,
                               "config_rpi.json")

    rpi_config = ConfigurationRpi(path_config)
    list_seq = obtenir_list_seq(dossier_config)

    json_config_rpi = rpi_config.get_config()
    btn_appuie = None
    if "appuie" in json_config_rpi:
        btn_appuie = json_config_rpi["appuie"]

    btn_relache = None
    if "relache" in json_config_rpi:
        btn_relache = json_config_rpi["relache"]

    # Module pour le son
    interact = LoaderInterraction.get_interraction(load_seq=True)

    # RaspberryPi
    rpi = RpiBoutons(cmd=interact.play_sound, cmd_next=interact.play_next)
    rpi.charge_config(btn_appuie, btn_relache=btn_relache)
    rpi.set_list_seq(list_seq)

    # Un son emi quand pret pour savoir si peut jouer
    interact.play_sound(0)

    rpi.run()


if __name__ == "__main__":
    #     gestion_bouton()
    #     func_with_events()
    lance()
