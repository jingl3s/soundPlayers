#-*-coding:utf8;-*-
#qpy:3
'''
@author: zorbac
'''

# license
#
# Producer 2017 zorbac at free.fr
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).

import curses
import os
import select
import sys

from sounds.sound_interraction import LoaderInterraction


def configure_curses():
    screen = curses.initscr()
    curses.noecho()  # @UndefinedVariable
    # curses.cbreak()
    curses.start_color()
    screen.keypad(1)    # delete this line
    # @UndefinedVariable
    # @UndefinedVariable
    # @UndefinedVariable
    curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_CYAN)
    highlightText = curses.color_pair(1)  # @UndefinedVariable @UnusedVariable
    normalText = curses.A_NORMAL  # @UndefinedVariable @UnusedVariable
    screen.border(0)
    curses.curs_set(0)  # @UndefinedVariable
    box = curses.newwin(22, 64, 1, 1)  # @UndefinedVariable
    box.keypad(1)
    box.box()
    box.addstr(14, 3, "YOU HAVE PRESSED: ")

    screen.refresh()    # delete this line
    box.refresh()

    return screen, box


def lance():

    sauve_env_term = os.environ['TERM']
    sauve_env_info = os.environ['TERMINFO']

    os.environ['TERM'] = 'linux'
    os.environ['TERMINFO'] = '/etc/terminfo'

    interact = LoaderInterraction(True)

    screen, box = configure_curses()

    x = box.getch()
    # while x != 27:
    while True:
        interact.play_sound(x)
    #     print("-----------------{0}".format(valeur))
    #     interact.play_next()
        while select.select([sys.stdin], [], [], 0.01) == ([sys.stdin], [], []):
            sys.stdin.read(1)
        box.erase()
        box.addstr(14, 3, "YOU HAVE PRESSED: " + str(x))
        screen.border(0)
        box.border(0)
        screen.refresh()  # delete this line
        box.refresh()     # delete this line
        x = box.getch()

    curses.endwin()  # @UndefinedVariable

    os.environ['TERM'] = sauve_env_term
    os.environ['TERMINFO'] = sauve_env_info

    exit()


if __name__ == "__main__":
    try:
        lance()
    except Exception as e:
        print(e)
