#-*-coding:utf8;-*-
# qpy:3
'''
@author: zorbac
'''

# license
#
# Producer 2017 zorbac at free.fr
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).

import time

import RPi.GPIO as gpio  # @UnresolvedImport

# sys.path.append(os.getcwd())
from sounds.sound_interraction import LoaderInterraction


def loop_gpio():

    PINIR = 18  # this should be 2 on a V2 RPi
    gpio.setmode(gpio.BCM)  # broadcom mode
    gpio.setup(PINIR, gpio.IN)

    # Make the two LED flash on and off forever
    valeur_defaut = gpio.input(PINIR)

    interact = LoaderInterraction()

    try:
        while True:
            valeur_lue = gpio.input(PINIR)
            if valeur_lue != valeur_defaut:
                valeur_defaut = valeur_lue
                if True == valeur_lue:
                    print('Input was {0}'.format(valeur_lue))
                    interact.play_next()
            time.sleep(0.01)
            ##

    except KeyboardInterrupt:
        gpio.cleanup()


def func_with_events():
    '''
    Source : http://hackaday.com/2017/05/30/diy-google-aiy/
    '''
    #!/bin/python
    # Simple script for shutting down the raspberry Pi at the press of a button.
    # by Inderpreet Singh

    import RPi.GPIO as GPIO
    import time
    import os

    # Use the Broadcom SOC Pin numbers
    # Setup the Pin with Internal pullups enabled and PIN in reading mode.
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(02, GPIO.IN, pull_up_down=GPIO.PUD_UP)

    # Our function on what to do when the button is pressed
    def Shutdown(channel):
        os.system("sudo shutdown -h now")

    # Add our function to execute when the button pressed event happens
    GPIO.add_event_detect(02, GPIO.FALLING, callback=Shutdown, bouncetime=2000)

    # Now wait!
    while 1:
        time.sleep(1)


if __name__ == '__main__':
    loop_gpio()
